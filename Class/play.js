
const youtubeAudioStream = require('ytdl-core');


module.exports = class Play {

  static match(message) {

    return message.content.startsWith('!play')

  }

  static action(message) {


    let voiceChannel = message.client.channels.cache
      .filter(function (channel) { return channel.type === 'voice' })
      .first()

    let args = message.content.split(' ')

    voiceChannel.join()
      .then(function (connection) {

        try {
      
          let stream = youtubeAudioStream(args[1])
          connection.play(stream).on('end', function () {

          })
        } catch (e) {
          message.reply('désolé vieux :/')
        }

      })
  }

}