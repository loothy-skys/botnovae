const YouTube = require('youtube-sr')
const youtubeAudioStream = require('ytdl-core');


module.exports = class srcYoutube {


    static match(message) {
        return message.content.startsWith('!src')

    }

    static action(message) {
        
        let voiceChannel = message.client.channels.cache
            .filter(function (channel) { return channel.type === 'voice' })
            .first()
        voiceChannel.join()
        .then(function(connection) {

            var searchUser = message.content
            var search = searchUser.slice(4)
            YouTube.search(search, { limit: 1 })
            .then(x => x.forEach(element =>{

                try {
                    message.reply('\n' + 'Titre : '+element.title + '\n' + 'temps :'+element.durationFormatted)
                   
                    let stream = youtubeAudioStream(' https://www.youtube.com/watch?v='+element.id)
                    connection.play(stream).on('end', function () {
        
                    })

                } catch (e) {
                    message.reply('désolé vieux :/')
                }

            }))
      
          
                 

        })

    }


}